<h1>Projet Anciens Élèves</h1>
Le site permet de lister les anciens élèves de l'EISTI pour obtenir des informations sur eux et en faire des statistiques.
Les informations sur les anciens élèves sont stockées dans un fichier JSON.

* Se connecter
	* En tant qu'ancien
		Les informations pour se connecter ne tant qu'élève sont dans le fichier JSON.
		Chaque ancien stocké peut se connecter avec le login et le password indiqués, et ainsi modifier ou supprimer ses informations via l'onglet *Mes données*
	* En tant qu'administrateur
		Le login de l'administrateur est admin et son mot de passe aussi.
		L'administrateur peut avoir accès à la liste des anciens élèves ayant autorisé l'utilisation de leurs données. Il peut également modifier leurs informations. Il a également accès à des statistiques à partir de ces données.

* Concernant la persistance des données
	Étant donné qu'il n'est pas possible de modifier les informations directement dans le fichier JSON, les modifications faites sont temporaires. Ainsi, si la page est actualisée, les données redeviennent les mêmes que celles du fichier d'origine.