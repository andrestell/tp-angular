import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RgpdComponent } from './rgpd/rgpd.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ConnexionComponent } from './connexion/connexion.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { StatistiquesComponent } from './statistiques/statistiques.component';
import { EditDataComponent } from './edit-data/edit-data.component';
import { ListeEtudiantsComponent } from './liste-etudiants/liste-etudiants.component';
import {AuthIntercepteur} from './services/interceptor';

@NgModule({
  declarations: [
    AppComponent,
    RgpdComponent,
    NavbarComponent,
    ConnexionComponent,
    StatistiquesComponent,
    EditDataComponent,
    ListeEtudiantsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthIntercepteur, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
