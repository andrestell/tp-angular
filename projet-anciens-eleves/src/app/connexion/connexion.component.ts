import { Component, OnInit } from '@angular/core';
import {ConnexionService} from '../services/connexion.service';
import {EtudiantsService} from '../services/etudiants.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css'],
})
export class ConnexionComponent implements OnInit {

  constructor(public connexion: ConnexionService, public etudiantsService: EtudiantsService, public user: UserService) { }

  ngOnInit() {
  }

  authentification(login, password) {
    this.user;
    this.etudiantsService.etudiants.forEach(etudiant => {
      if (login == etudiant['login'] && password == etudiant['password']) {
        this.connexion.connected = true;
        console.log('connecté');
        /*this.user.adresse = etudiant.adresse;
        this.user.dateNaissance = etudiant.dateNaissance;
        this.user.eleve = etudiant.eleve;
        this.user.email = etudiant.email;
        this.user.entreprise = etudiant.entreprise;
        this.user.nom = etudiant.nom;
        this.user.option = etudiant.option;
        this.user.password = etudiant.password;
        this.user.pays = etudiant.pays;
        this.user.prenom = etudiant.prenom;
        this.user.promotion = etudiant.promotion;
        this.user.salaire = etudiant.salaire;*/
        this.user.properties = etudiant;
      }
    });
    console.log('tentative d\'authentification');
    console.log(this.user);
  }

}
