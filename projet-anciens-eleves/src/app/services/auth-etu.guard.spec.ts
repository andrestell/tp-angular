import { TestBed, async, inject } from '@angular/core/testing';

import { AuthEtuGuard } from './auth-etu.guard';

describe('AuthEtuGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthEtuGuard]
    });
  });

  it('should ...', inject([AuthEtuGuard], (guard: AuthEtuGuard) => {
    expect(guard).toBeTruthy();
  }));
});
