import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EtudiantsService {

  etudiants: Array<object>;

  constructor(private  http: HttpClient) {
    // Gestion ajax
    this.http.get<Array<object>>('./assets/data/etudiants.json').subscribe(
      etu => {
        this.etudiants = etu;
      }
    );
  }
}
