import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';
import {ConnexionService} from './connexion.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthEditGuard implements CanActivate {
  constructor(private route: ActivatedRoute, public connexionService: ConnexionService, public userService: UserService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let idRoute = 0;
    console.log(this.route.url);
    /*this.route.params.subscribe(
      (p) => {
        idRoute = p.id;
        console.log(p);
      }
    );*/
    console.log(idRoute);
    return this.connexionService.connected && (!this.userService.properties['eleve'] || (this.userService.properties['id'] == idRoute));
  }

}
