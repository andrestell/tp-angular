import {Component, OnInit} from '@angular/core';
import {EtudiantsService} from '../services/etudiants.service';
import * as CanvasJS from '../../assets/canvajs.min';

@Component({
  selector: 'app-statistiques',
  templateUrl: './statistiques.component.html',
  styleUrls: ['./statistiques.component.css']
})
export class StatistiquesComponent implements OnInit {

  salaireMoyenStr;

  constructor(public etudiantsService: EtudiantsService) {
    let salaireMoyen = 0;
    let n = 0;
    etudiantsService.etudiants.forEach(etudiant => {
      if (etudiant['eleve'] && etudiant['authorized']) {
        salaireMoyen += parseInt(etudiant['salaire'].substr(0, 5), 10);
        n++;
      }
    });
    salaireMoyen = salaireMoyen / n;
    console.log(salaireMoyen);
    this.salaireMoyenStr = salaireMoyen.toString() + '€';
  }

  ngOnInit() {
    const tabStatPays = [];
    this.etudiantsService.etudiants.forEach(etudiant => {
      if (etudiant['eleve'] && etudiant['authorized']) {
        let test = false;
        tabStatPays.forEach(e => {
          if (etudiant['pays'] == e.name) {
            e.y += 1;
            test = true;
          }
        });
        if (!test) {
          tabStatPays.push({y: 1, name: etudiant['pays']});
        }
      }
    });


    const chart1 = new CanvasJS.Chart('chart1Container', {
      theme: 'light2',
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Répartition pays'
      },
      data: [{
        type: 'pie',
        showInLegend: true,
        toolTipContent: '<b>{name}</b>: ${y} (#percent%)',
        indexLabel: '{name} - #percent%',
        dataPoints: tabStatPays
      }]
    });
    chart1.render();

    const tabSalaireOption = [];
    const listOptions = [];
    const  listSalary = [];
    this.etudiantsService.etudiants.forEach((etudiant => {
      if (etudiant['eleve'] && etudiant['authorized']) {
        if (!listOptions.includes(etudiant['option'])) {
          listOptions.push(etudiant['option']);
        }
      }
    }));
    listOptions.forEach(option => {
      let n = 0;
      let sumSalary = 0;
      this.etudiantsService.etudiants.forEach(etudiant => {
        if (etudiant['eleve'] && etudiant['authorized']){
          if (option == etudiant['option']) {
            n++;
            sumSalary += parseInt(etudiant['salaire'].substr(0, 5), 10);
          }
        }
      });
      listSalary.push(sumSalary / n);
    });

    for (let i = 0; i < listOptions.length ; i++) {
      tabSalaireOption.push({y: listSalary[i], label: listOptions[i]});
    }

    console.log(tabSalaireOption);
    const chart2 = new CanvasJS.Chart('chart2Container', {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Salaire Moyen par Option'
      },
      data: [{
        type: 'column',
        dataPoints: tabSalaireOption
      }]
    });

    chart1.render();
    chart2.render();

  }

}
