import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EtudiantsService} from '../services/etudiants.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-edit-data',
  templateUrl: './edit-data.component.html',
  styleUrls: ['./edit-data.component.css']
})
export class EditDataComponent implements OnInit {

  id: number;

  constructor(private route: ActivatedRoute, public etuServ: EtudiantsService, public user: UserService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (p) => {
        this.id = +p.id;
      }
    );
  }

  edit(nom, prenom, mail, promo, option, pays, entreprise, salaire, authorized) {
    console.log(nom);
    this.etuServ.etudiants[this.id]['nom'] = nom;
    this.etuServ.etudiants[this.id]['prenom'] = prenom;
    this.etuServ.etudiants[this.id]['email'] = mail;
    this.etuServ.etudiants[this.id]['promotion'] = promo;
    this.etuServ.etudiants[this.id]['option'] = option;
    this.etuServ.etudiants[this.id]['pays'] = pays;
    this.etuServ.etudiants[this.id]['entreprise'] = entreprise;
    this.etuServ.etudiants[this.id]['salaire'] = salaire;
    this.etuServ.etudiants[this.id]['authorized'] = authorized;
  }

  delete() {
    console.log('supprime données utilisateur');
    this.etuServ.etudiants[this.id]['nom'] = "";
    this.etuServ.etudiants[this.id]['prenom'] = "";
    this.etuServ.etudiants[this.id]['email'] = "";
    this.etuServ.etudiants[this.id]['promotion'] = "";
    this.etuServ.etudiants[this.id]['option'] = "";
    this.etuServ.etudiants[this.id]['pays'] = "";
    this.etuServ.etudiants[this.id]['entreprise'] = "";
    this.etuServ.etudiants[this.id]['salaire'] = "";
    this.etuServ.etudiants[this.id]['authorized'] = false;
  }

}
