import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RgpdComponent} from './rgpd/rgpd.component';
import {ConnexionComponent} from './connexion/connexion.component';
import {StatistiquesComponent} from './statistiques/statistiques.component';
import {EditDataComponent} from './edit-data/edit-data.component';
import {ListeEtudiantsComponent} from './liste-etudiants/liste-etudiants.component';
import {AuthAdminGuard} from './services/auth-admin.guard';
import {AuthEtuGuard} from './services/auth-etu.guard';
import {AuthEditGuard} from './services/auth-edit.guard';


const routes: Routes = [
  {path: 'rgpd', component: RgpdComponent},
  {path: 'connexion', component: ConnexionComponent},
  {path: 'stats', component: StatistiquesComponent, canActivate: [AuthAdminGuard]},
  {path: 'liste', component: ListeEtudiantsComponent, canActivate: [AuthAdminGuard]},
  // {path: 'edit-data', component: EditDataComponent, canActivate: [AuthAdminGuard, AuthEtuGuard]},
  {path: 'edit-data/:id', component: EditDataComponent, canActivate: [AuthEditGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
