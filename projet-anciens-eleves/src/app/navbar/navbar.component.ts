import { Component, OnInit } from '@angular/core';
import {ConnexionService} from '../services/connexion.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public connexion: ConnexionService, public user: UserService) {
    console.log(connexion);
    console.log(user);
  }

  ngOnInit() {
  }

}
